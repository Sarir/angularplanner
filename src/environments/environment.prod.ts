export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCNBvrAz7pAvgjgBMSVY20Y46Mk8XpX8Oo',
    authDomain: 'angularplanner.firebaseapp.com',
    databaseURL: 'https://angularplanner.firebaseio.com',
    projectId: 'angularplanner',
    storageBucket: 'angularplanner.appspot.com',
    messagingSenderId: '1056718084547'
  }
};
