import { Component, OnInit } from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireAuth} from 'angularfire2/auth';
import {ModalService} from '../modal.service';
declare var $: any;

@Component({
  selector: 'app-edit-event-modal',
  templateUrl: './edit-event-modal.component.html'
})
export class EditEventModalComponent {

  yearValid = true;

  constructor(private fStore: AngularFirestore, private fAuth: AngularFireAuth, public sModal: ModalService) {}

  async editEvent(event) {
    event.preventDefault();

    const id = this.sModal.inputId;
    const title = this.sModal.inputTitle;
    const color = this.sModal.inputColor;
    const date = this.sModal.inputDate;
    const author = this.sModal.inputAuthor;

    const year = new Date(Date.parse(date)).getFullYear();

    if (isNaN(year) || year > 3000) {
      this.yearValid = false;
      setTimeout(() => this.yearValid = true, 2000);
      return;
    }

    let error = false;

    await this.fStore.collection('events').doc(id).set({
      id: id,
      author: author,
      title: title,
      color: color,
      date: date
    }).catch(err => {
      window.alert(err);
      error = true;
    });

    if (!error) {
      $('#editEventModal').modal('hide');
    }
  }

}
