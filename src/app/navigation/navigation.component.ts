import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit {

  constructor(public fAuth: AngularFireAuth) {}

  ngOnInit() {

  }

  logout() {
    this.fAuth.auth.signOut();
  }
}
