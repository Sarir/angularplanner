import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimerService {

  notificationPermission = false;

  timers = [];

  constructor() {
    Notification.requestPermission().then((data) => {
      this.notificationPermission = data === 'granted';
    });
  }

  resetTimers() {
    for (const timer of this.timers) {
      clearTimeout(timer);
    }
  }

  setupTimers(events) {

    this.resetTimers();

    const now = Date.now();

    for (const event of events) {
      const date = Date.parse(event.date);

      const ms = date - now;

      // skip notification timer
      // because ms can be 32 bit integer
      // https://stackoverflow.com/questions/3468607/why-does-settimeout-break-for-large-millisecond-delay-values
      if (ms > 2147483647) {
        continue;
      }

      if (ms > 0) {
        setTimeout(() => {
          if (this.notificationPermission) {
            const notif = new Notification(event.title, {
              body: 'Событие произошло'
            });
          }
          event.old = true;
        }, ms);
      } else {
        event.old = true;
      }
    }
  }
}
