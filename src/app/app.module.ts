import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import {environment} from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { AddEventModalComponent } from './add-event-modal/add-event-modal.component';
import {TimerService} from './timer.service';
import { EditEventModalComponent } from './edit-event-modal/edit-event-modal.component';
import {ModalService} from './modal.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    LoginComponent,
    RegistrationComponent,
    AddEventModalComponent,
    EditEventModalComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase, 'angularplanner'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path: 'login', component: LoginComponent},
      {path: 'registration', component: RegistrationComponent},
      {path: '', component: HomeComponent},
      {path: '**', redirectTo: ''}
    ])
  ],
  providers: [TimerService, ModalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
