import {Component} from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {AngularFirestore} from 'angularfire2/firestore';
import {TimerService} from '../timer.service';
import {ModalService} from '../modal.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent {

  private events = [];
  filteredEvents = [];

  searchInput = '';

  constructor(public fAuth: AngularFireAuth,
              private fStore: AngularFirestore,
              private timer: TimerService,
              private sModal: ModalService) {

    try {
      this.events = JSON.parse(localStorage.getItem('events'));
    } catch (err) {
      this.events = [];
    }


    fStore.collection('events').valueChanges().subscribe(data => {
      this.events = data;
      localStorage.clear();
      localStorage.setItem('events', JSON.stringify(this.events));

      this.filterEvents();

      this.timer.setupTimers(this.events);
    });
  }

  updateSearch(value: string) {
    this.searchInput = value;
    this.debounce(this.filterEvents(), 200, false);
  }

  filterEvents() {
    this.filteredEvents = this.events.filter((event) => {
      return event.title.indexOf(this.searchInput) > -1;
    });
  }

  async deleteEvent(index: number) {
    const doc = this.fStore.collection('events').doc(this.events[index].id);
    const result = await doc.delete();
  }

  editEvent(index: number) {
    const event = this.events[index];

    this.sModal.inputId = event.id;
    this.sModal.inputTitle = event.title;
    this.sModal.inputColor = event.color;
    this.sModal.inputAuthor = event.author;
    this.sModal.inputDate = event.date;


    $('#editEventModal').modal('show');
  }

  getDate(date: string) {
    return new Date(date);
  }

  debounce(func, wait, immediate) {
    let timeout;
    return function() {
      const context = this, args = arguments;
      const later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args);
        }
      };
      const callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) {
        func.apply(context, args);
      }
    };
  };

}
