import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AngularFireAuth} from 'angularfire2/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  error = false;
  errorMessage = '';

  constructor(private fAuth: AngularFireAuth, private router: Router) {}

  async login(event) {
    event.preventDefault();

    this.error = false;

    const email = event.target.querySelector('#inputEmail').value;
    const password = event.target.querySelector('#inputPassword').value;

    const result = await this.fAuth.auth.signInWithEmailAndPassword(email, password).catch(res => {
      this.errorMessage = res;
      this.error = true;
      setTimeout(() => {
        this.errorMessage = '';
        this.error = false;
      }, 3000);
    });

    if (!this.error) {
      this.router.navigate(['']);
    }

  }
}
