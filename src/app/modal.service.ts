import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  inputId = '';
  inputTitle = '';
  inputAuthor = '';
  inputColor = 'blue';
  inputDate = '';

  constructor() {}

  clear() {
    this.inputId = '';
    this.inputTitle = '';
    this.inputAuthor = '';
    this.inputColor = '';
    this.inputDate = '';
  }
}
