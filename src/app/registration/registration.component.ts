import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent {

  error = false;
  errorMessage = '';

  constructor(private fAuth: AngularFireAuth, private router: Router) {}

  async registration(event) {
    event.preventDefault();

    this.error = false;

    const email = event.target.querySelector('#inputEmail').value;
    const password = event.target.querySelector('#inputPassword').value;
    const cpassword = event.target.querySelector('#inputCPassword').value;

    if (password !== cpassword) {
      this.errorMessage = 'Passwords doesn\'t match';
      this.error = true;
      this.hideErrorMessage();
      return;
    }

    const result = await this.fAuth.auth.createUserWithEmailAndPassword(email, password).catch(res => {
      this.errorMessage = res;
      this.error = true;
      this.hideErrorMessage();
    });

    if (!this.error) {
      this.router.navigate(['']);
    }
  }

  hideErrorMessage() {
    setTimeout(() => {
      this.errorMessage = '';
      this.error = false;
    }, 3000);
  }
}
