import { Component, OnInit } from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';
import {AngularFireAuth} from 'angularfire2/auth';
import {ModalService} from '../modal.service';
declare var $: any;

@Component({
  selector: 'app-add-event-modal',
  templateUrl: './add-event-modal.component.html'
})
export class AddEventModalComponent {

  yearValid = true;

  constructor(private fStore: AngularFirestore, private fAuth: AngularFireAuth, public sModal: ModalService) {}

  async addEvent(event) {
    event.preventDefault();

    const year = new Date(Date.parse(this.sModal.inputDate)).getFullYear();

    if (isNaN(year) || year > 3000) {
      this.yearValid = false;
      setTimeout(() => this.yearValid = true, 2000);
      return;
    }

    const id = this.makeId();

    const eventObj = {
      id: id,
      author: this.fAuth.auth.currentUser.email,
      title: this.sModal.inputTitle,
      color: this.sModal.inputColor,
      date: this.sModal.inputDate
    };

    let error = false;

    const document = this.fStore.collection('events').doc(id);

    const result = await document.ref.set(eventObj).catch((err) => {
      document.delete();
      window.alert(err.message);
      error = true;
    });

    if (!error) {
      $('#addEventModal').modal('hide');
      this.sModal.clear();
    }
  }

  makeId(): string {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 15; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
  }

}
